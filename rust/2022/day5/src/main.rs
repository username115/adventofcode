
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

#[derive(Clone)]
struct Crate {
    title: char
}

struct Stack {
    col_center: usize, //used only for formatting. Indicates the index of the center character for the stack
    crates: Vec<Crate>
}

impl Clone for Stack {
    fn clone(&self) -> Stack {
        Stack {
            col_center: self.col_center,
            crates: self.crates.to_vec(),
        }
    }
}

fn move9000(supplies: &mut Vec<Stack>, count: i32, from_col: usize, to_col: usize) {
    for _ in 0..count {
        let moved_crate = supplies[from_col - 1].crates.pop().unwrap();
        supplies[to_col - 1].crates.push(moved_crate);
    }
}

fn move9001(supplies: &mut Vec<Stack>, count: i32, from_col: usize, to_col: usize) {
    let mut temp_vec: Vec<Crate> = Vec::new();
    for _ in 0..count {
        temp_vec.push(supplies[from_col - 1].crates.pop().unwrap());
    }

    while !temp_vec.is_empty() {
        supplies[to_col - 1].crates.push(temp_vec.pop().unwrap());
    }
}

fn process(mut supplies: Vec<Stack>, instructions: &Vec<String>, move_func: fn(&mut Vec<Stack>, i32, usize, usize)) {

    for inst in instructions {
        let mut args = inst.split_whitespace();

        let mut arg = args.next();

        let mut count = 0;
        let mut from_col = 0;
        let mut to_col = 0;

        while arg != None {
            let arg_str = arg.unwrap();
            if arg_str == "move" {
                arg = args.next();
                count = arg.unwrap().parse().expect("Columns must be numbers");
            } else if arg_str == "from" {
                arg = args.next();
                from_col = arg.unwrap().parse().expect("Columns must be numbers");
            } else if arg_str == "to" {
                arg = args.next();
                to_col = arg.unwrap().parse().expect("Columns must be numbers");
            }
            arg = args.next();
        }
        move_func(&mut supplies, count, from_col, to_col);
    }

    print_supplies(&supplies);

    println!("");
    println!("The tops of the stacks are {}", supplies.iter().fold(String::new(), |acc, col| format!("{acc}{}", match col.crates.last() { Some(c) => c.title, None => ' '})));

}

fn print_supplies(supplies: &Vec<Stack>) {
    let mut output: Vec<String> = Vec::new();

    let mut found_crate = true;
    let mut index = 0;

    output.push((1..=supplies.len()).fold(String::new(), |acc, col| format!("{acc} {col}  ")));

    while found_crate {
        let mut line = String::new();
        found_crate = false;
        for col in supplies {
            if col.crates.len() > index {
                line = format!("{line}[{}] ", col.crates[index].title);
                found_crate = true;
            } else {
                line = format!("{line}    ");
            }
        }
        index = index + 1;
        output.push(line);
    }

    while !output.is_empty() {
        println!("{}", output.pop().unwrap());
    }

}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    let mut supplies: Vec<Stack> = Vec::new();
    let mut instructions: Vec<String> = Vec::new();
    let mut temp_reader: Vec<String> = Vec::new();
    let mut reading_cargo = true;

    for line in reader.lines() {
        let line_string = line.unwrap();
        if reading_cargo {
            if line_string.trim().is_empty() {
                //finished reading cargo, load everything into the supplies structure
                let stack_num_string = temp_reader.pop().unwrap();
                for (i, c) in stack_num_string.char_indices() {
                    if c.is_numeric() {
                        supplies.push(Stack {
                            col_center: i,
                            crates: Vec::new(),
                        });
                    }
                }

                //We have our empty stacks now, lets fill them
                while !temp_reader.is_empty() {
                    let row = temp_reader.pop().unwrap();
                    for col in supplies.iter_mut() {
                        if row.as_bytes()[col.col_center].is_ascii_alphabetic() {
                            col.crates.push(Crate {
                                title: row.as_bytes()[col.col_center] as char
                            })
                        }
                    }
                }

                reading_cargo = false;

            } else {
                temp_reader.push(line_string);
            }
        } else {
            instructions.push(line_string);
        }
    }

    print_supplies(&supplies);

    process(supplies.to_vec(), &instructions, move9000);

    process(supplies, &instructions, move9001);

}
