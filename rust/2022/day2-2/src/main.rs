
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> = match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    let rock_score = 1;
    let paper_score = 2;
    let scissors_score = 3;

    let loss_score = 0;
    let draw_score = 3;
    let win_score = 6;

    let mut total_score = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        let mut game = line_string.split_whitespace();
        let opp = game.next().unwrap();
        let me = game.next().unwrap();

        total_score = total_score + match me {
            "X" => {
                loss_score + match opp {
                    "A" => scissors_score,
                    "B" => rock_score,
                    "C" => paper_score,
                    _ => 0
                }
            },
            "Y" => {
                draw_score + match opp {
                    "A" => rock_score,
                    "B" => paper_score,
                    "C" => scissors_score,
                    _ => 0
                }
            },
            "Z" => {
                win_score + match opp {
                    "A" => paper_score,
                    "B" => scissors_score,
                    "C" => rock_score,
                    _ => 0
                }
            },
            _ => 0
        }


    }

    println!("Total score: {}", total_score);

}
