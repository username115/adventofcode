
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    //read the input line for the drawn numbers
    let mut cur_count = 0;
    let mut cal_vec : Vec<i32> = Vec::new();
    let mut skip_blanks = true;

    for line in reader.lines() {
        let line_string = line.unwrap();
        if line_string.trim().len() == 0 {
            if !skip_blanks {
                cal_vec.push(cur_count);
                cur_count = 0;
                skip_blanks = true;
            }
        }
        else {
            skip_blanks = false;
            cur_count = cur_count + line_string.trim().parse::<i32>().expect("Calorie counts must be numbers!");
        }
    }

    cal_vec.sort_unstable();
    cal_vec.reverse();

    if cal_vec.len() >= 3 {
        println!("The total of the top 3 most calories is {}", cal_vec[0] + cal_vec[1] + cal_vec[2]);
    }
    else {
        println!("Not enough elements")
    }

}
