
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

fn find_start_marker(input: &String, num_chars: usize) -> i32 {

    let mut input_buf: Vec<char> = Vec::new();

    for (i, c) in input.char_indices() {
        input_buf.push(c);

        //println!("{:?}", input_buf);

        let mut dup_found = input_buf.len() < num_chars;

        if input_buf.len() >= num_chars {
            for (j, d) in input_buf.iter().enumerate() {
                let end = &input_buf[j+1..];
                for e in end {
                    if d == e {
                        dup_found = true;
                        //println!("Duplicate is {}", d);
                        break;
                    }
                }
                if dup_found {
                    break;
                }
            }
        }

        if !dup_found {
            return (i as i32) + 1;
        }

        while input_buf.len() >= num_chars {
            input_buf.remove(0);
        }

    }
    return -1;

}

fn part1(input: &String) {
    println!("Found start of packet marker at {}", find_start_marker(&input, 4));
}

fn part2(input: &String) {
    println!("Found start of message marker at {}", find_start_marker(&input, 14));
}

fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    //All input for this challenge is expected on one line
    let input = reader.lines().next().unwrap().unwrap();

    part1(&input);

    part2(&input);

}
