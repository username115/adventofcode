
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    let mut overlapping_lines = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();

        let mut pairs = line_string.split(',');
        let mut elf1 = pairs.next().unwrap().split('-');
        let mut elf2 = pairs.next().unwrap().split('-');
        let elf1_lower: i32 = elf1.next().unwrap().parse().unwrap();
        let elf1_upper: i32 = elf1.next().unwrap().parse().unwrap();
        let elf2_lower: i32 = elf2.next().unwrap().parse().unwrap();
        let elf2_upper: i32 = elf2.next().unwrap().parse().unwrap();

        //print!("{}", line_string);

        if (elf1_lower >= elf2_lower && elf1_lower <= elf2_upper)
            || (elf1_upper >= elf2_lower && elf1_upper <= elf2_upper)
            || (elf2_lower >= elf1_lower && elf2_upper <= elf1_upper)
            || (elf1_lower >= elf2_lower && elf1_upper <= elf2_upper) {
            overlapping_lines = overlapping_lines + 1;
            //print!(" overlap");
        }
        //println!("");
    }

    println!("Overlapping lines: {}", overlapping_lines);

}
