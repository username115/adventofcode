
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;
use lazy_static::lazy_static;
use std::collections::HashMap;


#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

lazy_static! {
    static ref PRIORITIES: HashMap<char, i32> = {
        let mut m = HashMap::new();
        for c in b'a'..=b'z' {
            m.insert(c as char, (c - b'a') as i32 + 1);
        }
        for c in b'A'..=b'Z' {
            m.insert(c as char, (c - b'A') as i32 + 27);
        }

        m
    };
}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> = match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    //println!("Priority of B: {}", PRIORITIES[&"B".chars().next().unwrap()]);

    let mut total_priority = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        let part1 = &line_string[..(line_string.len()/2)];
        let part2 = &line_string[(line_string.len()/2)..];
        //println!("part1: {}", part1);
        //println!("part2: {}", part2);
        for c in part1.chars() {
            if part2.contains(c) {
                total_priority = total_priority + PRIORITIES[&c];
                break;
            }
        }
    }

    println!("Total priority of mixed items is: {}", total_priority);

}
