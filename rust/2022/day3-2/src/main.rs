
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;
use lazy_static::lazy_static;
use std::collections::HashMap;


#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

lazy_static! {
    static ref PRIORITIES: HashMap<char, i32> = {
        let mut m = HashMap::new();
        for c in b'a'..=b'z' {
            m.insert(c as char, (c - b'a') as i32 + 1);
        }
        for c in b'A'..=b'Z' {
            m.insert(c as char, (c - b'A') as i32 + 27);
        }

        m
    };
}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> = match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    //println!("Priority of B: {}", PRIORITIES[&"B".chars().next().unwrap()]);

    let mut total_priority = 0;

    let mut line1: String = "".to_string();
    let mut line2: String = "".to_string();
    let mut line_count = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        line_count = line_count + 1;
        if line_count == 1 {
            line1 = line_string;
        }
        else if line_count == 2 {
            line2 = line_string;
        }
        else if line_count == 3 {
            for c in line1.chars() {
                if line2.contains(c) && line_string.contains(c) {
                    total_priority = total_priority + PRIORITIES[&c];
                    break;
                }
            }
            line_count = 0;
        }
    }

    println!("Total priority of mixed items is: {}", total_priority);

}
