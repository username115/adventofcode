
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> = match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    let rock_score = 1;
    let paper_score = 2;
    let scissors_score = 3;

    let loss_score = 0;
    let draw_score = 3;
    let win_score = 6;

    let mut total_score = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        let mut game = line_string.split_whitespace();
        let opp = game.next().unwrap();
        let me = game.next().unwrap();

        total_score = total_score + match me {
            "X" => {
                rock_score + match opp {
                    "A" => draw_score,
                    "B" => loss_score,
                    "C" => win_score,
                    _ => 0
                }
            },
            "Y" => {
                paper_score + match opp {
                    "A" => win_score,
                    "B" => draw_score,
                    "C" => loss_score,
                    _ => 0
                }
            },
            "Z" => {
                scissors_score + match opp {
                    "A" => loss_score,
                    "B" => win_score,
                    "C" => draw_score,
                    _ => 0
                }
            },
            _ => 0
        }


    }

    println!("Total score: {}", total_score);

}
