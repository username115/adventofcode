
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

struct Position {
    pub forward: i32,
    pub depth: i32
}

impl Position {
    fn new() -> Position {
        Position{forward:0, depth:0}
    }

    fn move_forward(&mut self, dist:i32) {
        self.forward = self.forward + dist;
    }

    fn move_up(&mut self, dist:i32) {
        self.depth = self.depth - dist; //Up is negative depth
    }

    fn move_down(&mut self, dist:i32) {
        self.depth = self.depth + dist; //Down is positive depth
    }
}



fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    let mut pos = Position::new();

    for line in reader.lines() {
        let line = line.unwrap();
        let mut full_command = line.split_whitespace();
        let command = full_command.next().unwrap();
        let dist = full_command.next().unwrap();
        let dist = dist.trim().parse().expect("Command arguments should be numbers");

        match command {
            "forward" => { pos.move_forward(dist); },
            "up" => { pos.move_up(dist); },
            "down" => { pos.move_down(dist); },
            _ => { panic!("Invalid command")}
        }
    }

    println!("Horizontal Distance: {}", pos.forward);
    println!("Depth: {}", pos.depth);
    println!("Product of position: {}", pos.forward * pos.depth);

}
