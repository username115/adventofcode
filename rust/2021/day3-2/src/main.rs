
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;
use std::vec::Vec;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

fn add_to_sum(sum: &mut Vec<i32>, line: String) {
    for (i, digit) in line.trim().chars().enumerate() {
        if digit == '1' {
            sum[i] = sum[i] + 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_to_sum() {
        let ln1 = String::from("00111");
        let ln2 = String::from("01001");
        let mut sums = vec![0;5];
        add_to_sum(&mut sums, ln1);
        add_to_sum(&mut sums, ln2);
        assert_eq!(sums[0], 0);
        assert_eq!(sums[1], 1);
        assert_eq!(sums[2], 1);
        assert_eq!(sums[3], 1);
        assert_eq!(sums[4], 2);
    }
}

fn main() {
    let args = Arguments::parse();
    
    let mut reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };


    let mut first_line = String::new();
    let result = reader.read_line(&mut first_line);
    if result.is_err() {
        println!("Failed to read input");
        return;
    }

    let num_digits = first_line.trim().len();

    let mut sums: Vec<i32>;
    let mut oxy_list: Vec<String> = Vec::new();
    let mut co2_list: Vec<String> = Vec::new();
    oxy_list.push(first_line.clone());
    co2_list.push(first_line);

    for line in reader.lines() {
        let line_string = line.unwrap();
        oxy_list.push(line_string.clone());
        co2_list.push(line_string.clone());
    }

    let mut i = 0;
    while oxy_list.len() > 1 {
        let threshold: i32 = i32::try_from(oxy_list.len() / 2).unwrap();
        sums = vec![0; num_digits];
        for line in oxy_list.iter() {
            add_to_sum(&mut sums, line.clone());
        }
        println!("Bit {}: {} entries left in O2 list.", i, oxy_list.len());

        let common_bit = 
        if sums[i] > threshold {
            '1'
        } else if sums[i] < threshold || (oxy_list.len() & 1) == 1 {
            '0'
        } else {
            println!("O2 Digit {} is equally common", i);
            '1'
        };
        println!("Keeping bits that are {}", common_bit);
        oxy_list.retain(|s| {s.chars().nth(i).unwrap() == common_bit});

        i = i + 1;
    }
    println!("");

    i = 0;
    while co2_list.len() > 1 {
        let threshold: i32 = i32::try_from(co2_list.len() / 2).unwrap();
        sums = vec![0; num_digits];
        for line in co2_list.iter() {
            add_to_sum(&mut sums, line.clone());
        }
        println!("Bit {}: {} entries left in CO2 list.", i, co2_list.len());

        let uncommon_bit = 
        if sums[i] > threshold {
            '0'
        } else if sums[i] < threshold || (co2_list.len() & 1) == 1 {
            '1'
        } else {
            println!("CO2 Digit {} is equally common", i);
            '0'
        };
        println!("Keeping bits that are {}", uncommon_bit);
        co2_list.retain(|s| {s.chars().nth(i).unwrap() == uncommon_bit});
        i = i + 1;
    }

    if oxy_list.len() != 1 {
        println!("Error: oxy_list not narrowed down to one value. Size is: {}", oxy_list.len());
        return;
    }
    if co2_list.len() != 1 {
        println!("Error: co2_list not narrowed down to one value. Size is: {}", co2_list.len());
        return;
    }

    let oxy = i32::from_str_radix(oxy_list.first().unwrap(), 2).unwrap();
    let co2 = i32::from_str_radix(co2_list.first().unwrap(), 2).unwrap();

    println!("oxy: {:02b}, CO2: {:02b}", oxy, co2);

    println!("{}", co2 * oxy);

}
