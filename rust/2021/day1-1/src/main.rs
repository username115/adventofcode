
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();
    
    let mut reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };
    let mut last_depth: i32;
    let mut line_string = String::new();
    match reader.read_line(&mut line_string) {
        Ok(_) => {last_depth = line_string.trim().parse().expect("Only numbers are valid inputs");}
        Err(_) => {last_depth = -1;}
    };
    if last_depth < 0 {
        return;
    }

    let mut num_increases = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        if line_string.trim().len() == 0 {
            break;
        }
        let depth: i32 = line_string.trim().parse().expect("Only numbers are valid inputs");
        if depth > last_depth {
            num_increases = num_increases + 1;
        }
        last_depth = depth;
    }

    println!("Depth increased {} times", num_increases);

}
