
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;
use std::vec::Vec;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

fn add_to_sum(sum: &mut Vec<i32>, line: String) {
    for (i, digit) in line.trim().chars().enumerate() {
        if digit == '1' {
            sum[i] = sum[i] + 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_to_sum() {
        let ln1 = String::from("00111");
        let ln2 = String::from("01001");
        let mut sums = vec![0;5];
        add_to_sum(&mut sums, ln1);
        add_to_sum(&mut sums, ln2);
        assert_eq!(sums[0], 0);
        assert_eq!(sums[1], 1);
        assert_eq!(sums[2], 1);
        assert_eq!(sums[3], 1);
        assert_eq!(sums[4], 2);
    }
}

fn main() {
    let args = Arguments::parse();
    
    let mut reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };


    let mut first_line = String::new();
    let result = reader.read_line(&mut first_line);
    if result.is_err() {
        return;
    }

    let num_digits = first_line.trim().len();

    let mut sums: Vec<i32> = vec![0; num_digits];
    let mut num_inputs = 1;

    for line in reader.lines() {
        let line_string = line.unwrap();
        add_to_sum(&mut sums, line_string);
        num_inputs = num_inputs + 1;
    }



    let threshold: i32 = num_inputs / 2;

    let mut gamma = 0;
    
    for (i, digit) in sums.iter().rev().enumerate() {
        //println!("digit {} count: {}", i, digit);
        if *digit > threshold {
            gamma = gamma + (1 << i);
        }
    }
    let epsilon = gamma ^ ((1 << num_digits) - 1);

    println!("digits: {}, threshold: {}", num_digits, threshold);

    println!("gamma: {:#02b}, epsilon: {:#02b}", gamma, epsilon);

    println!("{}", gamma * epsilon);

}
