

use ndarray::Array2;
//use ndarray::arr2;
use ndarray::Axis;
use string_builder::Builder;

pub struct BingoBoard {
    numbers: Array2<i32>,
    called: Array2<bool>
}

impl BingoBoard {
    pub fn new() -> BingoBoard {
        BingoBoard{
            numbers: Array2::<i32>::zeros((5, 5)),
            called: Array2::<bool>::default((5, 5))
        }
    }

    pub fn call_number(&mut self, num_called: i32) {
        for i in 0..self.numbers.len_of(Axis(0)) {
            for j in 0..self.numbers.len_of(Axis(1)) {
                if self.numbers[(i, j)] == num_called {
                    self.called[(i, j)] = true;
                }
            }
        };
    }

    pub fn get_board_as_string(self) -> String {
        let mut builder = Builder::new(105); //boards should be 105 characters long

        for i in 0..self.numbers.len_of(Axis(0)) {
            for j in 0..self.numbers.len_of(Axis(1)) {
                builder.append(if self.called[(i, j)] {"*"} else {" "});
                builder.append(format!("{:2}", self.numbers[(i, j)]));
            }
            builder.append("\n");
        };
        builder.string().unwrap()
    }

    pub fn set_row(&mut self, row_num: usize, row: String) {
        for (i, num) in row.split_whitespace().enumerate() {
            self.numbers[(row_num, i)] = num.parse().unwrap();
        }
    }
}