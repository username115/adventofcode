
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;
mod bingo;

use bingo::bingo::BingoBoard;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();
    
    let mut reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    //read the input line for the drawn numbers
    let mut drawn_numbers = String::new();
    let result = reader.read_line(&mut drawn_numbers);
    if result.is_err() {
        println!("Error reading input");
        return;
    }

    let mut boards: Vec<BingoBoard> = Vec::new();

    let mut row_num: usize = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        if line_string.trim().len() == 0 {
            row_num = 0;
            boards.push(BingoBoard::new());
        } else {
            boards.last_mut().unwrap().set_row(row_num, line_string);
            row_num = row_num + 1;
        }
    }

    for drawing in drawn_numbers.split_whitespace() {
        let num: i32 = drawing.parse().expect("Boards must contain only numbers");

        for board in boards.iter_mut() {
            board.call_number(num);
        }
    }

    for board in boards {
        println!("{}", board.get_board_as_string());
    }

}
