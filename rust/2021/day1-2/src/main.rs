
use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}

struct SlidingWindow {
    a: i32,
    b: i32,
    c: i32
}

impl SlidingWindow {
    fn new() -> SlidingWindow {
        SlidingWindow {a: -1, b: -1, c: -1}
    }

    fn is_valid(&self) -> bool {
        self.a >= 0 && self.b >= 0 && self.c >= 0
    }

    fn shift(&mut self, input: i32) {
        self.a = self.b;
        self.b = self.c;
        self.c = input;
    }

    fn sum(&self) -> i32 {
        self.a + self.b + self.c
    }
}



fn main() {
    let args = Arguments::parse();
    
    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };
    let mut last_depth = -1;
    let mut slider = SlidingWindow::new();

    let mut num_increases = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();
        if line_string.trim().len() == 0 {
            break;
        }
        let depth: i32 = line_string.trim().parse().expect("Only numbers are valid inputs");

        slider.shift(depth);
        print!("{}", depth);
        if slider.is_valid() {
            let depth = slider.sum();
            if last_depth >= 0 {
                print!(" sum {}", depth);
                if depth > last_depth {
                    print!(", increase\n");
                    num_increases = num_increases + 1;
                } else if depth == last_depth {
                    print!(", same\n");
                } else {
                    print!(", decrease\n");
                }
            }
            last_depth = depth;
        }
    }

    println!("Depth increased {} times", num_increases);

}
