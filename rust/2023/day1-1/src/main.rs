use clap::Parser;
use std::io::{self, BufReader, BufRead};
use std::fs;

#[derive(Parser,Default,Debug)]
struct Arguments
{
    input_file: Option<String>
}


fn main() {
    let args = Arguments::parse();

    let reader: Box<dyn BufRead> =  match args.input_file {
        Some(filename) => Box::new(BufReader::new(fs::File::open(filename).unwrap())),
        None => Box::new(BufReader::new(io::stdin()))
    };

    let mut sum = 0;

    for line in reader.lines() {
        let line_string = line.unwrap();

        let filtered: Vec<char> = line_string.chars().filter(|&c| c.is_digit(10)).collect();

        //if filtered.len() < 2 {
        //    panic!("Not enough digits in the input line {}", line_string);
        //}

        let first = filtered.first().unwrap().to_digit(10).unwrap();
        let last = filtered.last().unwrap().to_digit(10).unwrap();

        let line_num = first*10 + last;
        println!("{}", line_num);
        sum += line_num;
    }

    println!();
    println!("{}", sum);

}